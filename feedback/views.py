from django.shortcuts import render, redirect
from .models import FeedBackModel
from .forms import FeedBackForm

# Create your views here.

def list(request):
    feedbacks = FeedBackModel.objects.all()
    context = {
        'judul' : 'This is FeedBack',
        'feedbacks' : feedbacks
    }

    return render(request, 'feedback.html', context)

def create(request):
    feedback_form = FeedBackForm(request.POST or None)

    if request.method == "POST" :
        if feedback_form.is_valid() :
            feedback_form.save()

            return redirect('list-feedback')

    context = {
        'judul' : 'Create Feedback',
        'feedback_form' : feedback_form
    }

    return render(request, 'forms.html', context)

