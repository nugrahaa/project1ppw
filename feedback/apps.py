from django.apps import AppConfig #pragma: no cover


class FeedbackConfig(AppConfig): #pragma: no cover
    name = 'feedback'
