from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.utils import timezone
from feedback.models import FeedBackModel
from .views import list, create
from django.http import HttpRequest
from datetime import date
from .forms import FeedBackForm
import unittest


# Create your tests here.
class PepewStatusUnitTest(TestCase):
    # - - - - - - - - - - URL TEST - - - - - - - -
    def test_feedback_list_is_exist(self):
        response = Client().get('/feedback/list/')
        self.assertEqual(response.status_code,200)

    def test_feedback_create_is_exist(self):
        response = Client().get('/feedback/create/')
        self.assertEqual(response.status_code, 200)

    def test_invalid_url_not_found(self) :
        response = Client().get('/feedback/ariganteng/')
        self.assertEqual(response.status_code, 404)

    # - - - - - - - - TEMPLATE TEST - - - - - - - - -
    def test_feedback_page_using_func(self):
        found = resolve('/feedback/list/')
        self.assertEqual(found.func, list)

    def test_feedback_list_using_func(self):
        found = resolve('/feedback/create/')
        self.assertEqual(found.func, create)

    def test_feedback_list_page_uses_template(self):
        response = Client().get('/feedback/list/')
        self.assertTemplateUsed(response, 'feedback.html')

    def test_feedback_create_page_uses_template(self):
        response = Client().get('/feedback/create/')
        self.assertTemplateUsed(response, 'forms.html')
    
    # - - - - - - - - - MODEL TEST - - - - - - - - 
    def create_feedback(self, 
            nama_pengirim   = 'ari angga',
            komentar        = 'tdd seru banget',
            tanggal         = timezone.now(),
            rating          = '3'
        ):

        return FeedBackModel.objects.create(
            nama_pengirim=nama_pengirim,
            komentar=komentar,
            tanggal=tanggal,
            rating=rating
        )

    def test_feedback_creation(self):
        obj = self.create_feedback()
        self.assertTrue(isinstance(obj, FeedBackModel))
        self.assertEqual(obj.__str__(), obj.nama_pengirim)

    # - - - - - - - - - FORM TEST - - - - - - - - -  -
    def test_valid_form(self):
        obj = FeedBackModel.objects.create(
            nama_pengirim   = 'ari angga nugraha',
            komentar        = 'ayo kita test form dulu',
            tanggal         = timezone.now(),
            jenis_kelamin    = 'Perempuan',
            rating          = '1'
        )

        data = {
            'nama_pengirim' : obj.nama_pengirim,
            'komentar'      : obj.komentar,
            'jenis_kelamin' : obj.jenis_kelamin,
            'tanggal'       : obj.tanggal,
            'rating'        : obj.rating
        }

        form = FeedBackForm(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        obj = FeedBackModel.objects.create(
            nama_pengirim   = 'ari angga nugraha',
            komentar        = 'ayo kita test form dulu',
            tanggal         = timezone.now(),
            rating          = ''
        )

        data = {
            'nama_pengirim' : obj.nama_pengirim,
            'komentar'      : obj.komentar,
            'tanggal'       : obj.tanggal,
            'rating'        : obj.rating
        }

        form = FeedBackForm(data=data)
        self.assertFalse(form.is_valid())



    
    



