from django.db import models

class FeedBackModel(models.Model):
    nama_pengirim   = models.CharField(max_length=50)

    JENIS_KELAMIN = (
        ('Laki-laki', 'Laki-Laki'),
        ('Perempuan', 'Perempuan')
    )

    jenis_kelamin   = models.CharField(
        max_length = 100,
        choices = JENIS_KELAMIN,
        default = 'Perempuan',
    )
    komentar        = models.CharField(max_length=500)
    tanggal         = models.DateField(auto_now=True, auto_now_add=False)  
    rating          = models.CharField(max_length=1)

    def __str__(self):
        return self.nama_pengirim
    
