from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.utils import timezone
from buatDok.models import profilDok
from .views import buat_dok,list_dok
from django.http import HttpRequest
from .forms import buatDokForm
import unittest


# Create your tests here.
class BuatDokUnitTest(TestCase):
    # - - - - - - - - - - URL TEST - - - - - - - -
    def test_buatDok_create_is_exist(self):
        response = Client().get('/cariDok/create/')
        self.assertEqual(response.status_code,200)

    def test_buatDok_list_is_exist(self):
        response = Client().get('/cariDok/umum/')
        self.assertEqual(response.status_code, 200)

    def test_invalid_url_not_found(self) :
        response = Client().get('/cariDok/asqwe/mantap')
        self.assertEqual(response.status_code, 404)

    # - - - - - - - - TEMPLATE TEST - - - - - - - - -
    def test_buatDok_create_using_class(self):
        found = resolve('/cariDok/create/')
        self.assertEqual(found.view_name,'create-Dok')

    def test_buatDok_list_using_class(self):
        found = resolve('/cariDok/umum/')
        self.assertEqual(found.view_name, 'list-Dok')

    def test_buatDok_list_page_uses_template(self):
        response = Client().get('/cariDok/umum/')
        self.assertTemplateUsed(response, 'list_dok.html')

    def test_buatDok_create_page_uses_template(self):
        response = Client().get('/cariDok/create/')
        self.assertTemplateUsed(response, 'buat_dok.html')
    
    # - - - - - - - - - MODEL TEST - - - - - - - - 
    def create_profilDok(self, 
            nama   = 'Wasuu',
            umur = '10',
            jenis_kelamin  = 'laki-laki',
            spesialis = 'umum',
            pengalaman = '12',
            tempatPraktek = 'Condet',
            waktuAwal = '08:00',
            waktuAkhir = '10:00',
            day = 'Selasa',
        ):

        return profilDok.objects.create(
            nama=nama,
            umur=umur,
            jenis_kelamin=jenis_kelamin,
            spesialis=spesialis,
            pengalaman=pengalaman,
            tempatPraktek = tempatPraktek
        )

    # - - - - - - - - - FORM TEST - - - - - - - - -  -
    def test_valid_form_dok(self):
        obj = profilDok.objects.create(
            nama   = 'Wasuu',
            umur = '10',
            jenis_kelamin  = 'Laki-laki',
            spesialis = 'Umum',
            pengalaman = '12',
            tempatPraktek = 'Condet',
            waktuAwal = '16:00',
            waktuAkhir  = '18:00',
            day = 'Selasa',
        )

        data = {
            'nama' : obj.nama,
            'umur' : obj.umur,
            'jenis_kelamin' : obj.jenis_kelamin,
            'spesialis': obj.spesialis,
            'pengalaman' : obj.pengalaman,
            'tempatPraktek' : obj.tempatPraktek,
            'waktuAwal' : obj.waktuAwal,
            'waktuAkhir' : obj.waktuAkhir,
            'day' : obj.day,
        }
        
        form = buatDokForm(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form_dok(self):
        obj = profilDok.objects.create(
            nama   = 'Wasuu',
            umur = '10',
            jenis_kelamin  = 'Laki-laki',
            spesialis = 'Umum',
            pengalaman = '0',
            tempatPraktek = '',
            waktuAwal = '16:00',
            waktuAkhir  = '18:00',
            day = 'Selasa',
        )

        data = {
            'nama' : obj.nama,
            'umur' : obj.umur,
            'jenis_kelamin' : obj.jenis_kelamin,
            'spesialis': obj.spesialis,
            'pengalaman' : obj.pengalaman,
            'tempatPraktek' : obj.tempatPraktek,
            'waktuAwal' : obj.waktuAwal,
            'waktuAkhir' : obj.waktuAkhir,
            'day' : obj.day,
        }

        form = buatDokForm(data=data)
        self.assertFalse(form.is_valid())
