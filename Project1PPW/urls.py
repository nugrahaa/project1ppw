from django.contrib import admin
from django.urls import path, include

from . import views
from caridok.views import listPesanan

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.homepage),
    path('feedback/', include('feedback.urls')),
    path('cariDok/', include('caridok.urls')),
    path('cariDok/',include('buatDok.urls')),
    path('tanyadok/', include('tanyadok.urls')),
    path('listPesanan/', listPesanan, name='listPesanan')
]

