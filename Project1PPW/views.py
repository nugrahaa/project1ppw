from django.shortcuts import render

def homepage(request):
    context = {
        'judul' : 'Dokterin - Solusi Kesehatan',
    }

    return render(request, 'homepage.html', context)