from django.urls import path,include
from . import views


urlpatterns = [
    path('', views.cariPenyakit, name='cariPenyakit'),
    path('pesenDokter/', views.pesenDokter, name = 'pesenDokter'),
]
