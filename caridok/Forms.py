from django import forms
from .models import PemesananModel

class PemesananForm(forms.ModelForm):
    class Meta:
        model = PemesananModel
        fields = [
            'nama_pemesan',
            'no_hp',
            'alamat',
            'pilihan_waktu',
        ]
        widgets = {
            'nama_pemesan' : forms.TextInput(attrs={
                'class' : 'form-control',
                'placeholder':'ari jelek'}
                ),
            'no_hp': forms.TextInput(attrs={
                'class' : 'form-control',
                'placeholder':'0812345679'}
                ),
            'alamat' : forms.TextInput(attrs={
                'class' : 'form-control',
                'placeholder':'Kutek'}
                ),
            'pilihan_waktu ' : forms.TextInput(attrs={
                'class' : 'form-control',
                'placeholder':'Senin'}
                ),
        }
