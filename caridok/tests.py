from django.test import TestCase
from django.test import Client
from django.urls import resolve
from caridok.models import PemesananModel
from .views import cariPenyakit, pesenDokter
from .Forms import PemesananForm
import unittest


# Create your tests here.

class SimpleTest(TestCase):
    def test_project_homepage_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'homepage.html')
class CariDokUnitTest(TestCase):
    # - - - - - - - - - - URL TEST - - - - - - - -
    def test_caridok_cari_penyakit_is_exist(self):
        response = Client().get('/cariDok/')
        self.assertEqual(response.status_code,200)

    def test_caridok_pesen_dokter_is_exist(self):
        response = Client().get('/cariDok/pesenDokter/')
        print(response.status_code)
        self.assertEqual(response.status_code, 200)

    def test_invalid_url_not_found(self) :
        response = Client().get('/cariDok/bahkanbahkin/asdkl')
        self.assertEqual(response.status_code, 404)

    # - - - - - - - - TEMPLATE TEST - - - - - - - - -
    def test_caridok_cari_penyakit_using_func(self):
        found = resolve('/cariDok/')
        self.assertEqual(found.func, cariPenyakit)

    def test_caridok_pesen_dokter_using_func(self):
        found = resolve('/cariDok/pesenDokter/')
        self.assertEqual(found.func, pesenDokter)

    def test_caridok_cari_penyakit_page_uses_template(self):
        response = Client().get('/cariDok/')
        self.assertTemplateUsed(response, 'cariPenyakit.html')

    def test_caridok_pesen_dokter_page_uses_template(self):
        response = Client().get('/cariDok/pesenDokter/')
        self.assertTemplateUsed(response, 'pesenDokter.html')

     # - - - - - - - - MODEL TEST - - - - - - - - -

    def create_pesanan(self, 
            nama_pemesan   = 'ari jelek',
            no_hp       = '081234567890',
            alamat         = 'sadas dasad asdsa',
            pilihan_waktu          = 'Senin 00 00 00 00'
        ):

        return PemesananModel.objects.create(
            nama_pemesan=nama_pemesan,
            no_hp=no_hp,
            alamat=alamat,
            pilihan_waktu=pilihan_waktu
        )

    def test_pesanan_creation(self):
        obj = self.create_pesanan()
        self.assertTrue(isinstance(obj, PemesananModel))
        self.assertEqual(obj.__str__(), obj.nama_pemesan)

    
    # - - - - - - - - FORM TEST - - - - - - - - -

    def test_valid_form(self):
        obj = PemesananModel.objects.create(
            nama_pemesan    = 'ari jelek',
            no_hp           = '081234567890',
            alamat          = 'sadas dasad asdsa',
            pilihan_waktu   = 'Senin 00 00 00 00'
        )

        data = {
            'nama_pemesan'  : obj.nama_pemesan,
            'no_hp'         : obj.no_hp,
            'alamat'        : obj.alamat,
            'pilihan_waktu'  : obj.pilihan_waktu
        }

        form = PemesananForm(data=data)
        print(form.errors)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        obj = PemesananModel.objects.create(
            nama_pemesan    = 'ari jelek',
            no_hp           = '081234567890',
            alamat          = '',
            pilihan_waktu   = ''
        )

        data = {
            'nama_pemesan'  : obj.nama_pemesan,
            'no_hp'         : obj.no_hp,
            'alamat'        : obj.alamat,
            'pilihan_waktu'  : obj.pilihan_waktu
        }

        form = PemesananForm(data=data)
        self.assertFalse(form.is_valid())



