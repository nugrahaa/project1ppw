from django.shortcuts import render, redirect
from .Forms import PemesananForm
from .models import PemesananModel 

# Create your views here.

def cariPenyakit(request):
    return render(request, 'cariPenyakit.html')

def listPesanan(request):
    pesanans = PemesananModel.objects.all()
    context = {
        'judul' : 'List Pesanan',
        'pesanans' : pesanans
    }

    return render(request, 'listPesanan.html', context)
    
def pesenDokter(request):
    pesen_form = PemesananForm(request.POST or None)
    if request.method == 'POST':
        if pesen_form.is_valid():
            pesen_form.save()

            return redirect('listPesanan')

    context = {
        'pesen_form' : pesen_form
    }

    return render(request,'pesenDokter.html',context)

